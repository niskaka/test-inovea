import { Component, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Model } from 'src/app/models/model';

@Component({
  selector: 'app-dialog-model-edition',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose
  ],
  templateUrl: './dialog-model-edition.component.html',
  styleUrl: './dialog-model-edition.component.scss'
})
export class DialogModelEditionComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogModelEditionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Model
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
