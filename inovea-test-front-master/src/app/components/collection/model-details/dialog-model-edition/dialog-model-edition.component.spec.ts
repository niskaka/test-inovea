import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogModelEditionComponent } from './dialog-model-edition.component';

describe('DialogModelEditionComponent', () => {
  let component: DialogModelEditionComponent;
  let fixture: ComponentFixture<DialogModelEditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DialogModelEditionComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DialogModelEditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
