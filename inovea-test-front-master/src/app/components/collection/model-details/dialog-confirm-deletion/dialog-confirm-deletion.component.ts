import { Component, Inject } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInput, MatInputModule } from '@angular/material/input';
import { Model } from 'src/app/models/model';
@Component({
  selector: 'app-dialog-confirm-deletion',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose
  ],
  templateUrl: './dialog-confirm-deletion.component.html',
  styleUrl: './dialog-confirm-deletion.component.scss'
})
export class DialogConfirmDeletionComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogConfirmDeletionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Model
  ) { }

  canUDelete: boolean = false;
  controlDeletion = new FormControl('');


  onNoClick(): void {
    this.dialogRef.close();
  }


  /**
   * Méthode permettant d'afficher le bouton "supprimer" quand l'utilisateur
   * recopie le nom du modèle en question
   */
  compareFieldsBeforeDeletion(): void {
    console.log(this.controlDeletion.getRawValue());
    this.canUDelete = this.controlDeletion.getRawValue() === this.data.name;
  }
}
