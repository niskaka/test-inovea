import { MatButtonModule } from '@angular/material/button';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {MatDividerModule} from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatDialog } from '@angular/material/dialog';
import { FrenchDatePipe } from 'src/app/pipes/french-date.pipe';
import { DialogModelEditionComponent } from './dialog-model-edition/dialog-model-edition.component';
import { GlobalService } from 'src/app/services/global.service';
import { DialogConfirmDeletionComponent } from './dialog-confirm-deletion/dialog-confirm-deletion.component';
import { Model } from 'src/app/models/model';

@Component({
  selector: 'app-model-details',
  standalone: true,
  imports: [
    MatCardModule,
    FrenchDatePipe,
    MatDividerModule,
    MatButtonModule
  ],
  templateUrl: './model-details.component.html',
  styleUrl: './model-details.component.scss'
})
export class ModelDetailsComponent implements OnInit{

  @Input() the_model: any;

  @Input() mes_modeles: Model[] = [];
  @Output() mes_modelesChange: EventEmitter<Model[]> = new EventEmitter();

  constructor(
    public dialog: MatDialog,
    private serviceAPI: GlobalService
  ) { }

  ngOnInit(): void {

  }

  /**
   * Cette méthode ouvre une fenêtre de dialog avec
   * le formulaire d'édition du modèle affiché en détails
   */
  openDialog(): void {
    const dialogRef = this.dialog.open(DialogModelEditionComponent, {
      restoreFocus: false,
      data: {
        name: this.the_model.name,
        polygons: this.the_model.polygons,
        author: this.the_model.author,
        description: this.the_model.description
      }
    });


    dialogRef.afterClosed().subscribe(result => {
      result.date = new Date();
      this.serviceAPI.updateModel(this.the_model.id, result).subscribe(putResult => {
        this.the_model = putResult;
      });
    });
  }

  /**
   * Cette méthode ouvre une fenêtre de dialog avec
   * un formmulaire permettant de confirmer la suppression d'un modèle
   */
  openDialogDeletion(): void {
    const dialogRef = this.dialog.open(DialogConfirmDeletionComponent, {
      restoreFocus: false,
      data: {
        name: this.the_model.name
      }
    });


    dialogRef.afterClosed().subscribe(data => {
      if (data !== undefined && data.name === this.the_model.name) {
        this.deleteModele();
      }
    });
  }


  /**
   * Cette méthode permet de supprimer dynamiquement le modèle afficher en détails
   * via l'API fournie
   */
  deleteModele() {
    this.serviceAPI.deleteModel(this.the_model.id).subscribe(deleted => {
      this.mes_modeles.splice(this.mes_modeles.indexOf(this.the_model), 1);
      this.mes_modelesChange.emit(this.mes_modeles);
      this.the_model = null;
    });
  }
}
