import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAddModelFormComponent } from './dialog-add-model-form.component';

describe('DialogAddModelFormComponent', () => {
  let component: DialogAddModelFormComponent;
  let fixture: ComponentFixture<DialogAddModelFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DialogAddModelFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DialogAddModelFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
