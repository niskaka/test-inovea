import { Component, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Model } from 'src/app/models/model';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'app-dialog-add-model-form',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    MatIconModule
  ],
  templateUrl: './dialog-add-model-form.component.html',
  styleUrl: './dialog-add-model-form.component.scss'
})
export class DialogAddModelFormComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogAddModelFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Model
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
