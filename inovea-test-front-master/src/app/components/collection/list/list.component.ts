import { FrenchDatePipe } from './../../../pipes/french-date.pipe';
import { Component, EventEmitter, Input, LOCALE_ID, Output } from '@angular/core';
import {MatDividerModule} from '@angular/material/divider';
import { Model } from 'src/app/models/model';
import { GlobalService } from 'src/app/services/global.service';
import { MatListModule } from '@angular/material/list';
import { NgScrollbarModule } from 'ngx-scrollbar';

@Component({
  selector: 'app-list',
  standalone: true,
  imports: [
    MatDividerModule,
    FrenchDatePipe,
    MatListModule,
    NgScrollbarModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR'}
  ],
  templateUrl: './list.component.html',
  styleUrl: './list.component.scss'
})
export class ListComponent {

  @Input() mes_modeles: Model[] = [];
  @Input() the_model: any;

  @Output() the_modelChange: EventEmitter<Model> = new EventEmitter();

  constructor(
    private serviceAPI: GlobalService
  ){}

  ngOnInit(): void {
  }


  /**
   *
   * @param id -> l'id du modèle sur lequel on a cliqué
   *
   * Méthode permettant d'afficher les détails d'un modèle via l'API fournie
   */
  openDetails(id: string) {
    this.serviceAPI.getModelById(id).subscribe(model => {
      this.the_model = model;
      this.the_modelChange.emit(model);
    });
  }

}
