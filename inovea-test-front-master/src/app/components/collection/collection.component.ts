import { MatInputModule } from '@angular/material/input';
import { Component, EventEmitter, Output } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { MatIconModule } from '@angular/material/icon';
import { ListComponent } from './list/list.component';
import { ModelDetailsComponent } from './model-details/model-details.component';
import { Model } from 'src/app/models/model';
import { GlobalService } from 'src/app/services/global.service';
import { map, scheduled, startWith, switchMap } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { DialogAddModelFormComponent } from './dialog-add-model-form/dialog-add-model-form.component';

@Component({
  selector: 'app-collection',
  standalone: true,
  imports: [
    MatToolbarModule,
    ListComponent,
    ModelDetailsComponent,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatAutocompleteModule,],
  templateUrl: './collection.component.html',
  styleUrl: './collection.component.scss'
})
export class CollectionComponent {

  @Output() filteredModelsChange: EventEmitter<Model[]> = new EventEmitter();

  modelName = new FormControl('');

  mes_modeles: Model[] = [];
  filteredModels: Model[] = [];

  modelClicked: any;

  constructor(
    private apiService: GlobalService,
    public dialog: MatDialog
  ){}

  ngOnInit(): void {

    // Récupération des données de l'API
    this.apiService.getList().subscribe(listModels => {
      this.mes_modeles = listModels;
      this.filteredModels = listModels;
    });

    // contrôle si l'utilisateur utilise la recherche
    this.modelName.valueChanges.subscribe(val => {
      if ( val !== null )
        this.search(val)
    });


  }

  /**
   * @param val
   * Méthode de recherche par valeur
   * Filtre la liste des modèles selon le paramètre val
   */
  private search(val: string): void {

    this.filteredModels = [];
    for (const m of this.mes_modeles) {
      if (m.modelName.includes(val)) {
        this.filteredModels.push(m);
      }
    }
  }

  /**
   *
   * Ouvre une fenêtre modal avec le formulaire d'ajout d'un modèle
   */
  public openDialog(): void {

    const dialogRef = this.dialog.open(DialogAddModelFormComponent, {
      restoreFocus: false,
      width: '45%',
      panelClass: 'custom-dialog',
      data: {}
    });


    dialogRef.afterClosed().subscribe(data => {
      if (data !== undefined && data !== null) {
        this.createModel(data);
      }

    });
  }

  /**
   *
   * @param data -> les données du formulaire d'ajout d'un modèle
   *
   * Méthode permettant d'ajouter un modèle via l'API fournie
   */
  private createModel(data: any) {
    if (data.name !== undefined && data.name != null && data.author !== undefined && data.author != null && data.polygons !== undefined && data.polygons != null) {
      this.apiService.createModel(data).subscribe(result => {
        console.log(result);
        this.modelClicked = result;
        this.filteredModels.push(this.modelClicked);
        this.filteredModelsChange.emit(this.filteredModels);
      });
    } else {
      alert("Les champs 'Nom du modèle', 'Nombre de polygones', et 'Auteur du modèle' sont obligatoires !");
    }

  }






}
