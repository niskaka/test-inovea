export interface Model {

  id: string

  name: string;

  // nombre de polygones qui composent le modèle
  polygons: number;

  author: string;

  date: string;

  description: string;

  // utiliser pour récupérer les images, ne pas changer, utiliser la propriété name à la place
  modelName: string;

};
