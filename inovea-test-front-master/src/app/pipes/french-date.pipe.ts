import { formatDate } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'frenchDate',
  standalone: true
})
export class FrenchDatePipe implements PipeTransform {

  //ADD DATE FORMAT TO FRENCH LOCALE
  transform(value: string | number | Date, ...args: unknown[]): unknown {
    return formatDate(value, 'EEEE d MMMM YYYY', 'fr-FR');
  }

}
