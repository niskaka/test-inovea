import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Model } from '../models/model';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  private apiRoute = "https://inov-test-api.onrender.com/api";

  constructor(private http: HttpClient) { }

  //PING API SERVER
  pingServer(): Observable<string>{
    return this.http.get<string>(`${this.apiRoute}/ping`);
  }

  //GET LIST
  getList(): Observable<Model[]> {
    return this.http.get<Model[]>(`${this.apiRoute}/models`);
  }

  //GET MODEL BY ID
  getModelById(id: string): Observable<Model> {
    return this.http.get<Model>(`${this.apiRoute}/models/${id}`);
  }

   //POST REQUEST
  createModel(body: JSON): Observable<JSON> {
    return this.http.post<JSON>(`${this.apiRoute}/models`, body);
  }

  //PUT REQUEST
  updateModel(id: string, body: JSON): Observable<JSON> {
    return this.http.put<JSON>(`${this.apiRoute}/models/${id}`, body);
  }

  //DELETE REQUEST
  deleteModel(id: string): Observable<string> {
    return this.http.delete<string>(`${this.apiRoute}/models/${id}`);
  }
}
